/*
 *
 * Easy creation of simple screensaver for web pages.
 *
 * @example $("#screensaver").jScreenSaver(
 *                          delay: 5000,
 *                          ignore: "#navigation",
 *                          events: "mousedown keydown"                  
 *                          );
 *
 */
(function($){

	//Just simple screensaver for webpages
	function JScreenSaver(el, options) {

		//Defaults:
		this.defaults = {
			delay: 5000,
			ignore: '#ignoreme',
			events: 'mousemove mousedown keydown',
			rv: false
		};

		//Extending options:
		this.opts = $.extend({}, this.defaults, options);

		//Privates:
		this.$el = $(el);
		this.counter;
	}
	
	// Separate functionality from object creation
	JScreenSaver.prototype = {

		init: function() {
			var _this = this;
			_this.$el.hide();
			_this.startCounter();
		    _this.bindEvents();
		},

		//show the screensaver ( or reverse )
		show: function(obj) {
			var _this = obj;
			if(_this.opts.rv)
			    _this.$el.fadeOut("slow");
			else
			    _this.$el.fadeIn("slow");
		},

		//hide the screensaver ( or reverse ) 
		hide: function(obj) {
			var _this = obj;
			if(_this.opts.rv)
			   _this.$el.fadeIn("slow");
			else
			    _this.$el.fadeOut("slow");
			_this.resetCounter();
		},

		//start a counter
		startCounter: function() {
			var _this = this;
			_this.counter = setInterval($.proxy(function() { this.show(this); }, _this), _this.opts.delay);
		},

		//reset the counter
		resetCounter: function() {
			var _this = this;
            clearInterval(_this.counter);
            _this.startCounter();
		},

		//bind events to the elements
		bindEvents: function() {
			var _this = this;
            $(_this.opts.ignore).bind(_this.opts.events, function(){return false;});
            $(window).bind(_this.opts.events,$.proxy(function() { this.hide(this); }, _this));
		    _this.$el.bind(_this.opts.events,$.proxy(function() { this.hide(this); }, _this));
		}
	};

	// The actual plugin
	$.fn.jScreenSaver = function(options) {
		if(this.length) {
			    $(this).hide();
				var rev = new JScreenSaver(this, options);
				rev.init();
				$(this).data('jScreenSaver', rev);
			
		}
		return $(this);
	};
})(jQuery);

